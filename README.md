# tf-aws-usm-anywhere-sensor
AlienVault USM sensor deployment. Adapted from the CloudFormation template and [documentation](https://cybersecurity.att.com/documentation/usm-anywhere/deployment-guide/aws/about-usm-aws-sensor-deployment.htm?tocpath=Documentation%7CUSM%20Anywhere%E2%84%A2%7CUSM%20Anywhere%20Deployment%20Guide%7CUSM%20Anywhere%20Sensor%20Deployments%7CAWS%C2%A0Sensor%20Deployment%7C_____0).

## Usage
Example usage with traffic mirroring enabled along with a public IP:

```
module "usm" {
  source = "/path/to/module"

  name     = "usm-anywhere-sensor"
  key_name = "ssh-keypair"

  vpc_id                      = "vpc-0700258703b941875"
  subnet_id                   = "subnet-0461fcac5275c24c8"
  associate_public_ip_address = true
  traffic_mirroring           = true
}
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
#### Requirements

No requirements.

#### Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

#### Modules

No modules.

#### Resources

| Name | Type |
|------|------|
| [aws_ebs_volume.usm_anywhere_sensor_data](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ebs_volume) | resource |
| [aws_iam_instance_profile.usm_anywhere_sensor](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile) | resource |
| [aws_iam_policy.usm_anywhere_sensor](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.usm_anywhere_sensor](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.usm_anywhere_sensor](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_instance.usm_anywhere_sensor](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance) | resource |
| [aws_lb.usm_anywhere_traffic_mirror_target](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb) | resource |
| [aws_lb_listener.usm_anywhere_traffic_mirror_target](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_listener) | resource |
| [aws_lb_target_group.usm_anywhere_traffic_mirror_target](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_target_group) | resource |
| [aws_lb_target_group_attachment.usm_anywhere_traffic_mirror_target](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_target_group_attachment) | resource |
| [aws_network_interface.usm_anywhere_traffic_mirroring](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_interface) | resource |
| [aws_network_interface_attachment.usm_anywhere_traffic_mirroring](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_interface_attachment) | resource |
| [aws_security_group.usm_anywhere_connection](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group.usm_anywhere_log_services](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group.usm_anywhere_log_services_sensor](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group.usm_anywhere_traffic_interface](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group.usm_anywhere_traffic_mirroring](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group_rule.usm_anywhere_connection_egress](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.usm_anywhere_graylog](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.usm_anywhere_healthcheck](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.usm_anywhere_http](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.usm_anywhere_nxlog](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.usm_anywhere_ssh](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.usm_anywhere_syslog](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.usm_anywhere_tcp_syslog](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.usm_anywhere_tls_syslog](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.usm_anywhere_vxlan](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.usm_anywhere_vxlan_cidr](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.usm_anywhere_vxlan_egress](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.usm_anywhere_wsman](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_volume_attachment.usm_anywhere_sensor_data](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/volume_attachment) | resource |
| [aws_iam_policy_document.ec2_assume](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.usm_anywhere_sensor](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_network_interface.usm_anywhere_traffic_mirror_target](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/network_interface) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

#### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_associate_public_ip_address"></a> [associate\_public\_ip\_address](#input\_associate\_public\_ip\_address) | If you choose to deploy your sensor with a public IP address, the subnet you select must have 'Auto-assign public IPv4 address' enabled | `bool` | `false` | no |
| <a name="input_disable_api_termination"></a> [disable\_api\_termination](#input\_disable\_api\_termination) | API termination protection | `string` | `true` | no |
| <a name="input_egress_cidrs"></a> [egress\_cidrs](#input\_egress\_cidrs) | The IP address ranges that are allowed from the USM Anywhere Sensor | `list(string)` | <pre>[<br>  "0.0.0.0/0"<br>]</pre> | no |
| <a name="input_http_access_cidrs"></a> [http\_access\_cidrs](#input\_http\_access\_cidrs) | The IP address ranges that can be used to access the USM Anywhere Sensor that you are deploying in your AWS Account through the UI. For security considerations, 0.0.0.0/0 is not recommended, so please restrict to a smaller IP range if possible | `list(string)` | `[]` | no |
| <a name="input_key_name"></a> [key\_name](#input\_key\_name) | Name of an existing EC2 key pair to enable SSH access to your USM Anywhere Sensor | `string` | n/a | yes |
| <a name="input_name"></a> [name](#input\_name) | Name to assign created resources | `string` | n/a | yes |
| <a name="input_ssh_access_cidrs"></a> [ssh\_access\_cidrs](#input\_ssh\_access\_cidrs) | The IP address ranges that can be used to access the USM Anywhere Sensor that you are deploying in your AWS Account through the CLI. For security considerations, 0.0.0.0/0 is not recommended, so please restrict to a smaller IP range if possible | `list(string)` | `[]` | no |
| <a name="input_subnet_id"></a> [subnet\_id](#input\_subnet\_id) | ID of an existing subnet (for the primary network) in your Virtual Private Cloud (VPC) | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Map of tags to add to resources | `map(string)` | `{}` | no |
| <a name="input_traffic_mirroring"></a> [traffic\_mirroring](#input\_traffic\_mirroring) | Whether or not deploy the USM Anywhere sensor ready to use traffic mirroring. This option will deploy a m5.xlarge and a second network interface | `bool` | `false` | no |
| <a name="input_traffic_mirroring_nlb"></a> [traffic\_mirroring\_nlb](#input\_traffic\_mirroring\_nlb) | Whether to create an NLB to use as the traffic mirror target | `bool` | `false` | no |
| <a name="input_traffic_mirroring_source_cidr_blocks"></a> [traffic\_mirroring\_source\_cidr\_blocks](#input\_traffic\_mirroring\_source\_cidr\_blocks) | CIDR blocks to allow ingress vxlan traffic to the sensor. Used when `traffic_mirroring` and `traffic_mirroring_nlb` are `true`. | `list(string)` | `[]` | no |
| <a name="input_usm_anywhere_ami_id"></a> [usm\_anywhere\_ami\_id](#input\_usm\_anywhere\_ami\_id) | USM Anywhere Sensor AMI ID | `map(string)` | <pre>{<br>  "ap-northeast-1": "ami-082cfe3915cbc1f4d",<br>  "ap-northeast-2": "ami-029d77897905c0196",<br>  "ap-south-1": "ami-04e7f26009d268dd1",<br>  "ap-southeast-1": "ami-0d9205fa6f77e78ad",<br>  "ap-southeast-2": "ami-03f8400110b7ea908",<br>  "ca-central-1": "ami-00878bfeb9ed68ce7",<br>  "eu-central-1": "ami-0a543e742f241ec8e",<br>  "eu-west-1": "ami-023a1dedd8074c35b",<br>  "eu-west-2": "ami-07dbcdd7f2b92a94c",<br>  "eu-west-3": "ami-0635bbd0234247820",<br>  "sa-east-1": "ami-06aa4bca8df48a38d",<br>  "us-east-1": "ami-0bb2e019555924ae7",<br>  "us-west-1": "ami-018003358b5db13be",<br>  "us-west-2": "ami-0e8f1a691671cccd0"<br>}</pre> | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | ID of your existing Virtual Private Cloud (VPC) | `string` | n/a | yes |

#### Outputs

| Name | Description |
|------|-------------|
| <a name="output_connection_sg_id"></a> [connection\_sg\_id](#output\_connection\_sg\_id) | Security group for inbound connections assigned to the sensor |
| <a name="output_log_services_sensor_sg_id"></a> [log\_services\_sensor\_sg\_id](#output\_log\_services\_sensor\_sg\_id) | Security group for log services assigned to the sensor |
| <a name="output_log_services_sg_id"></a> [log\_services\_sg\_id](#output\_log\_services\_sg\_id) | Assign this Security Group to the instance you want to allow Syslog UDP/TCP/TLS and Graylog connectivity to your USM Sensor |
| <a name="output_traffic_mirroring_nlb_arn"></a> [traffic\_mirroring\_nlb\_arn](#output\_traffic\_mirroring\_nlb\_arn) | ARN of the traffic mirroring NLB |
| <a name="output_traffic_mirroring_sg_id"></a> [traffic\_mirroring\_sg\_id](#output\_traffic\_mirroring\_sg\_id) | Assign this Security Group to the instance you want to allow Traffic Mirroring connectivity to your USM Sensor Traffic Network Interface |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
