locals {
  tags = merge(
    {
      Name = var.name
    },
    var.tags
  )
}

resource "aws_lb" "usm_anywhere_traffic_mirror_target" {
  count = var.traffic_mirroring && var.traffic_mirroring_nlb ? 1 : 0

  name               = var.name
  load_balancer_type = "network"
  internal           = true
  subnets            = [var.subnet_id]

  tags = local.tags
}

data "aws_network_interface" "usm_anywhere_traffic_mirror_target" {
  count = var.traffic_mirroring && var.traffic_mirroring_nlb ? 1 : 0

  filter {
    name   = "description"
    values = ["ELB ${aws_lb.usm_anywhere_traffic_mirror_target[0].arn_suffix}"]
  }

  filter {
    name   = "subnet-id"
    values = [var.subnet_id]
  }
}

resource "aws_lb_target_group" "usm_anywhere_traffic_mirror_target" {
  count = var.traffic_mirroring && var.traffic_mirroring_nlb ? 1 : 0

  name        = var.name
  port        = 4789
  protocol    = "UDP"
  target_type = "ip"
  vpc_id      = var.vpc_id

  health_check {
    protocol = "HTTP"
    port     = 80
    path     = "/api/2.0/status"
  }

  tags = local.tags
}

resource "aws_lb_target_group_attachment" "usm_anywhere_traffic_mirror_target" {
  count = var.traffic_mirroring && var.traffic_mirroring_nlb ? 1 : 0

  target_group_arn = aws_lb_target_group.usm_anywhere_traffic_mirror_target[0].arn
  target_id        = sort(aws_network_interface.usm_anywhere_traffic_mirroring[0].private_ips)[0]
  port             = 4789
}

resource "aws_lb_listener" "usm_anywhere_traffic_mirror_target" {
  count = var.traffic_mirroring && var.traffic_mirroring_nlb ? 1 : 0

  load_balancer_arn = aws_lb.usm_anywhere_traffic_mirror_target[0].arn
  port              = 4789
  protocol          = "UDP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.usm_anywhere_traffic_mirror_target[0].arn
  }

  tags = local.tags
}

resource "aws_instance" "usm_anywhere_sensor" {
  ami           = var.usm_anywhere_ami_id[data.aws_region.current.name]
  instance_type = var.traffic_mirroring ? "m5.xlarge" : "m5.large"
  key_name      = var.key_name

  subnet_id = var.subnet_id
  vpc_security_group_ids = [
    aws_security_group.usm_anywhere_connection.id,
    aws_security_group.usm_anywhere_log_services_sensor.id
  ]
  associate_public_ip_address = var.associate_public_ip_address

  iam_instance_profile    = aws_iam_instance_profile.usm_anywhere_sensor.name
  disable_api_termination = var.disable_api_termination

  user_data = jsonencode({
    "nodeName" : var.name,
    "environment" : "prod",
    "av_profile" : "sensor",
    "av_resources" : "wyns"
  })

  root_block_device {
    encrypted = true
  }

  tags = local.tags
}

resource "aws_ebs_volume" "usm_anywhere_sensor_data" {
  availability_zone = aws_instance.usm_anywhere_sensor.availability_zone
  size              = 100
  type              = "gp2"
  encrypted         = true

  tags = local.tags

  lifecycle {
    ignore_changes = [availability_zone]
  }
}

resource "aws_volume_attachment" "usm_anywhere_sensor_data" {
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.usm_anywhere_sensor_data.id
  instance_id = aws_instance.usm_anywhere_sensor.id
}

resource "aws_network_interface" "usm_anywhere_traffic_mirroring" {
  count = var.traffic_mirroring ? 1 : 0

  description     = "Interface for VPC Traffic Mirroring"
  subnet_id       = var.subnet_id
  security_groups = [aws_security_group.usm_anywhere_traffic_interface[0].id]

  tags = local.tags
}

resource "aws_network_interface_attachment" "usm_anywhere_traffic_mirroring" {
  count = var.traffic_mirroring ? 1 : 0

  instance_id          = aws_instance.usm_anywhere_sensor.id
  network_interface_id = aws_network_interface.usm_anywhere_traffic_mirroring[0].id
  device_index         = 1
}

resource "aws_iam_instance_profile" "usm_anywhere_sensor" {
  name = var.name
  role = aws_iam_role.usm_anywhere_sensor.name
}

resource "aws_iam_role" "usm_anywhere_sensor" {
  name               = var.name
  assume_role_policy = data.aws_iam_policy_document.ec2_assume.json
}

data "aws_iam_policy_document" "ec2_assume" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "usm_anywhere_sensor" {
  name   = var.name
  policy = data.aws_iam_policy_document.usm_anywhere_sensor.json
}

data "aws_iam_policy_document" "usm_anywhere_sensor" {
  statement {
    actions = [
      "guardduty:Get*",
      "guardduty:List*",
      "cloudtrail:Describe*",
      "cloudtrail:Get*",
      "cloudtrail:List*",
      "cloudwatch:Describe*",
      "cloudwatch:Get*",
      "cloudwatch:List*",
      "logs:Describe*",
      "logs:Get*",
      "logs:TestMetricFilter",
      "ec2:Describe*",
      "elasticloadbalancing:Describe*",
      "iam:List*",
      "iam:Get*",
      "rds:Describe*",
      "s3:Get*",
      "s3:List*"
    ]

    resources = ["*"]
  }
}

resource "aws_iam_role_policy_attachment" "usm_anywhere_sensor" {
  policy_arn = aws_iam_policy.usm_anywhere_sensor.arn
  role       = aws_iam_role.usm_anywhere_sensor.name
}

resource "aws_security_group" "usm_anywhere_log_services" {
  name        = "${var.name}-log-services"
  description = "Enable USM Log Services. Assign this Security Group to the instance you want to allow Syslog UDP/TCP/TLS and Graylog connectivity to your USM Sensor"
  vpc_id      = var.vpc_id
}

resource "aws_security_group" "usm_anywhere_traffic_mirroring" {
  name        = "${var.name}-traffic-mirroring"
  description = "Enable USM Traffic Mirroring. Assign this Security Group to the instance you want to allow Traffic Mirroring connectivity to your USM Sensor Traffic Network Interface"
  vpc_id      = var.vpc_id
}

resource "aws_security_group" "usm_anywhere_traffic_interface" {
  count = var.traffic_mirroring ? 1 : 0

  name        = "${var.name}-traffic-interface"
  description = "Enable USM Traffic Mirror Connectivity on your USM Sensor Traffic Network Interface"
  vpc_id      = var.vpc_id
}

resource "aws_security_group_rule" "usm_anywhere_vxlan_egress" {
  count = var.traffic_mirroring ? 1 : 0

  description       = "Outbound communication for all traffic"
  type              = "egress"
  from_port         = -1
  to_port           = -1
  protocol          = -1
  security_group_id = aws_security_group.usm_anywhere_traffic_interface[0].id
  cidr_blocks       = var.egress_cidrs
}

resource "aws_security_group_rule" "usm_anywhere_vxlan" {
  count = var.traffic_mirroring ? 1 : 0

  description              = "Inbound traffic mirroring traffic"
  type                     = "ingress"
  from_port                = 4789
  to_port                  = 4789
  protocol                 = "udp"
  security_group_id        = aws_security_group.usm_anywhere_traffic_interface[0].id
  source_security_group_id = aws_security_group.usm_anywhere_traffic_mirroring.id
}

resource "aws_security_group_rule" "usm_anywhere_vxlan_cidr" {
  count = var.traffic_mirroring && var.traffic_mirroring_nlb ? 1 : 0

  description       = "Inbound traffic mirroring traffic"
  type              = "ingress"
  from_port         = 4789
  to_port           = 4789
  protocol          = "udp"
  security_group_id = aws_security_group.usm_anywhere_traffic_interface[0].id
  cidr_blocks       = var.traffic_mirroring_source_cidr_blocks
}

resource "aws_security_group_rule" "usm_anywhere_healthcheck" {
  count = var.traffic_mirroring && var.traffic_mirroring_nlb ? 1 : 0

  description       = "NLB health check"
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  security_group_id = aws_security_group.usm_anywhere_traffic_interface[0].id
  cidr_blocks       = ["${data.aws_network_interface.usm_anywhere_traffic_mirror_target[0].private_ip}/32"]
}

resource "aws_security_group" "usm_anywhere_connection" {
  name        = "${var.name}-connection"
  description = "Enable SSH and HTTP connectivity on your USM Sensor Instance"
  vpc_id      = var.vpc_id
}

resource "aws_security_group_rule" "usm_anywhere_connection_egress" {
  description       = "Outbound communication for all traffic"
  type              = "egress"
  from_port         = -1
  to_port           = -1
  protocol          = -1
  security_group_id = aws_security_group.usm_anywhere_connection.id
  cidr_blocks       = var.egress_cidrs
}

resource "aws_security_group_rule" "usm_anywhere_http" {
  count = length(var.http_access_cidrs) > 0 ? 1 : 0

  description       = "Inbound communication for HTTP traffic"
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  security_group_id = aws_security_group.usm_anywhere_connection.id
  cidr_blocks       = var.http_access_cidrs
}

resource "aws_security_group_rule" "usm_anywhere_ssh" {
  count = length(var.ssh_access_cidrs) > 0 ? 1 : 0

  description       = "Inbound communication for SSH traffic"
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  security_group_id = aws_security_group.usm_anywhere_connection.id
  cidr_blocks       = var.ssh_access_cidrs
}

resource "aws_security_group" "usm_anywhere_log_services_sensor" {
  name        = "${var.name}-log-services-sensor"
  description = "Enable Syslog UDP/TCP/TLS and Graylog connectivity in your USM Sensor Instance"
  vpc_id      = var.vpc_id
}

resource "aws_security_group_rule" "usm_anywhere_syslog" {
  description              = "USM Anywhere collects data through syslog over UDP on port 514 by default"
  type                     = "ingress"
  from_port                = 514
  to_port                  = 514
  protocol                 = "udp"
  security_group_id        = aws_security_group.usm_anywhere_log_services_sensor.id
  source_security_group_id = aws_security_group.usm_anywhere_log_services.id
}

resource "aws_security_group_rule" "usm_anywhere_tcp_syslog" {
  description              = "Inbound communication for reliable syslog service. USM Anywhere collects data through syslog over TCP on port 601/602 by default. (Internal, VPC to VPC)"
  type                     = "ingress"
  from_port                = 601
  to_port                  = 602
  protocol                 = "tcp"
  security_group_id        = aws_security_group.usm_anywhere_log_services_sensor.id
  source_security_group_id = aws_security_group.usm_anywhere_log_services.id
}

resource "aws_security_group_rule" "usm_anywhere_tls_syslog" {
  description              = "USM Anywhere collects data through syslog over TLS on port 6514/6515 by default"
  type                     = "ingress"
  from_port                = 6514
  to_port                  = 6515
  protocol                 = "tcp"
  security_group_id        = aws_security_group.usm_anywhere_log_services_sensor.id
  source_security_group_id = aws_security_group.usm_anywhere_log_services.id
}

resource "aws_security_group_rule" "usm_anywhere_graylog" {
  description              = "Inbound communication for Graylog Extended Log Format (GELF)"
  type                     = "ingress"
  from_port                = 12201
  to_port                  = 12201
  protocol                 = "udp"
  security_group_id        = aws_security_group.usm_anywhere_log_services_sensor.id
  source_security_group_id = aws_security_group.usm_anywhere_log_services.id
}

resource "aws_security_group_rule" "usm_anywhere_nxlog" {
  description              = "Inbound WBEM WS-Management HTTP over TLS/SSL (NXLog)"
  type                     = "ingress"
  from_port                = 5986
  to_port                  = 5986
  protocol                 = "tcp"
  security_group_id        = aws_security_group.usm_anywhere_log_services_sensor.id
  source_security_group_id = aws_security_group.usm_anywhere_log_services.id
}

resource "aws_security_group_rule" "usm_anywhere_wsman" {
  description              = "Inbound WBEM WS-Management HTTP over TLS/SSL (NXLog)"
  type                     = "ingress"
  from_port                = 5987
  to_port                  = 5987
  protocol                 = "tcp"
  security_group_id        = aws_security_group.usm_anywhere_log_services_sensor.id
  source_security_group_id = aws_security_group.usm_anywhere_log_services.id
}
