output "log_services_sg_id" {
  description = "Assign this Security Group to the instance you want to allow Syslog UDP/TCP/TLS and Graylog connectivity to your USM Sensor"
  value       = aws_security_group.usm_anywhere_log_services.id
}

output "traffic_mirroring_sg_id" {
  description = "Assign this Security Group to the instance you want to allow Traffic Mirroring connectivity to your USM Sensor Traffic Network Interface"
  value       = aws_security_group.usm_anywhere_traffic_mirroring.id
}

output "connection_sg_id" {
  description = "Security group for inbound connections assigned to the sensor"
  value       = aws_security_group.usm_anywhere_connection.id
}

output "log_services_sensor_sg_id" {
  description = "Security group for log services assigned to the sensor"
  value       = aws_security_group.usm_anywhere_log_services_sensor.id
}

output "traffic_mirroring_nlb_arn" {
  description = "ARN of the traffic mirroring NLB"
  value       = aws_lb.usm_anywhere_traffic_mirror_target.*.arn
}
