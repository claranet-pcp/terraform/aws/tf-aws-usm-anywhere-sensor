variable "name" {
  type        = string
  description = "Name to assign created resources"
}

variable "key_name" {
  type        = string
  description = "Name of an existing EC2 key pair to enable SSH access to your USM Anywhere Sensor"
}

variable "traffic_mirroring" {
  type        = bool
  description = "Whether or not deploy the USM Anywhere sensor ready to use traffic mirroring. This option will deploy a m5.xlarge and a second network interface"
  default     = false
}

variable "traffic_mirroring_nlb" {
  type        = bool
  description = "Whether to create an NLB to use as the traffic mirror target"
  default     = false
}

variable "traffic_mirroring_source_cidr_blocks" {
  type        = list(string)
  description = "CIDR blocks to allow ingress vxlan traffic to the sensor. Used when `traffic_mirroring` and `traffic_mirroring_nlb` are `true`."
  default     = []
}

variable "vpc_id" {
  type        = string
  description = "ID of your existing Virtual Private Cloud (VPC)"
}

variable "subnet_id" {
  type        = string
  description = "ID of an existing subnet (for the primary network) in your Virtual Private Cloud (VPC)"
}

variable "associate_public_ip_address" {
  type        = bool
  description = "If you choose to deploy your sensor with a public IP address, the subnet you select must have 'Auto-assign public IPv4 address' enabled"
  default     = false
}

variable "http_access_cidrs" {
  type        = list(string)
  description = "The IP address ranges that can be used to access the USM Anywhere Sensor that you are deploying in your AWS Account through the UI. For security considerations, 0.0.0.0/0 is not recommended, so please restrict to a smaller IP range if possible"
  default     = []
}

variable "ssh_access_cidrs" {
  type        = list(string)
  description = "The IP address ranges that can be used to access the USM Anywhere Sensor that you are deploying in your AWS Account through the CLI. For security considerations, 0.0.0.0/0 is not recommended, so please restrict to a smaller IP range if possible"
  default     = []
}

variable "egress_cidrs" {
  type        = list(string)
  description = "The IP address ranges that are allowed from the USM Anywhere Sensor"
  default     = ["0.0.0.0/0"]
}

variable "disable_api_termination" {
  type        = string
  description = "API termination protection"
  default     = true
}

variable "usm_anywhere_ami_id" {
  type        = map(string)
  description = "USM Anywhere Sensor AMI ID"
  default = {
    us-east-1      = "ami-0bb2e019555924ae7"
    us-west-2      = "ami-07606b287af65c338"
    us-west-1      = "ami-018003358b5db13be"
    us-west-2      = "ami-0e8f1a691671cccd0"
    eu-west-1      = "ami-023a1dedd8074c35b"
    eu-west-2      = "ami-07dbcdd7f2b92a94c"
    eu-west-3      = "ami-0635bbd0234247820"
    eu-central-1   = "ami-0a543e742f241ec8e"
    ap-southeast-1 = "ami-0d9205fa6f77e78ad"
    ap-southeast-2 = "ami-03f8400110b7ea908"
    ap-northeast-1 = "ami-082cfe3915cbc1f4d"
    ap-northeast-2 = "ami-029d77897905c0196"
    ap-south-1     = "ami-04e7f26009d268dd1"
    sa-east-1      = "ami-06aa4bca8df48a38d"
    ca-central-1   = "ami-00878bfeb9ed68ce7"
  }
}

variable "tags" {
  type        = map(string)
  description = "Map of tags to add to resources"
  default     = {}
}
